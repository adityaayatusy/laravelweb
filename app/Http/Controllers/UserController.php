<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $data = $user->all();
        return view('admin/users',['menu' => 'users', 'submenu' => 'users', 'data' => $data]);
    }


    public function create()
    { 
        $roles = Role::all();
        return view('admin/addEditUser',['menu' => 'users', 'submenu' => 'add user', 'roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {   
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role;

        if($user->save()){
            return redirect(route('users.index'))->with(['success' => 'Berhasil Menyimpan Data']);
        }else{
            return redirect(route('users.index'))->with(['failed' => 'Gagal Menyimpan Data']);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = new User();
        $roles = Role::all();
        $data = $user->where('id',$id)->first();
        return view('admin/addEditUser',['menu' => 'users', 'submenu' => 'edit user', 'data' => $data, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, User $u)
    {   
        $getPw = $u->find($id); 
        $user = User::find($id);
        if(isset($request->oldpassword)){
            if(Hash::check($request->oldpassword,$getPw->password)){
                $user->password = Hash::make($request->password);
            }else{
                return redirect(route('users.edit', $id))->with(['failed' => 'Password lama anda salah']);
            }
        }
        $user->name = $request->name;
        $user->email = $request->email;
        
        $user->role_id = $request->role;

        if($user->save()){
            return redirect(route('users.index'))->with(['success' => 'Berhasil Mengupdate Data']);
        }else{
            return redirect(route('users.edit', $id))->with(['failed' => 'Gagal Mengupdate Data']); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $a = User::find($id);
        dd($a);
        if($a->delete()){
            return redirect(route('users.index'))->with(['success' => 'Berhasil Menghapus Data']);
        }else{
            return redirect(route('users.index'))->with(['failed' => 'Gagal Menghapus Data']);
        }
    }
}
