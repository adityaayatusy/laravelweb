<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\User;
use App\Category;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Article $article)
    {
        $data = $article->all();
        return view('admin/articles',['menu' => 'articles', 'submenu' => 'articles', 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $category = Category::all();
        return view('admin/addEditArticle',['menu' => 'articles', 'submenu' => 'add article','category'=>$category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Article $article)
    {
    //    dd($request);
    //    thumbnail.jpg
    // dd($request->file('thumbnail'));
    if($request->hasFile('thumbnail')){
        $file = $request->file('thumbnail');
        $fileName = uniqid() . '.' . $file->getClientOriginalExtension();
        $file->move('img', $fileName);

    }else{
        $fileName = 'thumbnail.jpg';
    }

        if(isset($request->comment)){
            $comment = true;
        }else{
            $comment = false;
        }


    $slug = $this->createSlug($request->title);
        $article->slug = $slug;
        $article->title = $request->title;
        $article->content = $request->isi;
        $article->status = $request->status;
        $article->tag = $request->tag;
        $article->comment = $comment;
        $article->thumbnail = $fileName;
        $article->create_by = Auth::id();
        
        $article->category_id = $request->category;

        if($article->save()){
            return redirect(route('post.index'))->with(['success' => 'Berhasil Menyimpan Data']);
        }else{
            return redirect(route('post.index'))->with(['failed' => 'Gagal Menyimpan Data']);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Article::where('slug', $id)->first();
        
        return view('single',['data' => $data,'menu' => 'articles']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Article $article)
    {
        $category = Category::all();
        $data = $article->where('id',$id)->first();
        return view('admin/addEditArticle',['menu' => 'articles', 'submenu' => 'edit article', 'data' => $data, 'category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Article $article)
    {
        $a = $article->find($id);
        if($request->hasFile('thumbnail')){
            $file = $request->file('thumbnail');
            $fileName = uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('img', $fileName);
            $a->thumbnail = $fileName;
        }
    
        if(isset($request->comment)){
            $comment = true;
        }else{
            $comment = false;
        }
    
    
        $slug = $this->createSlug($request->title);

        $a->slug = $slug;
        $a->title = $request->title;
        $a->content = $request->isi;
        $a->status = $request->status;
        $a->tag = $request->tag;
        $a->comment = $comment;
        $a->create_by = Auth::id();
            
        $a->category_id = $request->category;

        if($a->save()){
            return redirect(route('post.index'))->with(['success' => 'Berhasil Mengubah Data']);
        }else{
            return redirect(route('post.index'))->with(['failed' => 'Gagal Mengubah Data']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $a = Article::find($id);
        if($a->delete()){
            return redirect(route('post.index'))->with(['success' => 'Berhasil Menghapus Data']);
        }else{
            return redirect(route('post.index'))->with(['failed' => 'Gagal Menghapus Data']);
        }
    }

    public function restore($id)
    {
        $a = Article::withTrashed()->where('id', $id);

        if($a->restore()){
            return redirect(route('post.trash'))->with(['success' => 'Berhasil Mengembalikan Data']);
        }else{
            return redirect(route('post.trash'))->with(['failed' => 'Gagal Mengembalikan Data']);
        }
    }


    public function permanentDestroy($id)
    {
        $a = Article::withTrashed()->find($id);

        if($a->forceDelete()){
            return redirect(route('post.trash'))->with(['success' => 'Berhasil Menghapus Permanent Data']);
        }else{
            return redirect(route('post.trash'))->with(['failed' => 'Gagal Menghapus Permanent Data']);
        }
    }

    //view category
    public function category(){
        $data = Category::all();
        return view('admin/category',['menu' => 'articles', 'submenu' => 'category', 'data' => $data]);
    }

    //store category
    public function storeCategory(Request $request, Category $category){
        
        $category->category_name = $request->category;
        if($category->save()){
            return redirect(route('post.category'))->with(['success' => 'Berhasil Mengubah Data']);
        }else{
            return redirect(route('post.category'))->with(['failed' => 'Gagal Mengubah Data']);
        }
        
    }

    //delete category
    public function destroyCategory($id){
        $c = Category::find($id);
        if($c->delete()){
            ///back to default
            Article::where('category_id', $id)->update(['category_id' => 0]);

            return redirect(route('post.category'))->with(['success' => 'Berhasil Menghapus Data']);
        }else{
            return redirect(route('post.category'))->with(['failed' => 'Gagal Menghapus Data']);
        }
    }

    //view trash
    public function trash(){
        $data = Article::onlyTrashed()->get();
        return view('admin/articles',['menu' => 'articles', 'submenu' => 'trash articles', 'data' => $data]);
    }

    public function createSlug($title, $id = 0)
    {
  
        $slug = explode(' ', $title);
        $slug = implode("-", $slug);


        $allSlugs = $this->getRelatedSlugs($slug, $id);

        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }

        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Article::select('slug')->where('slug', 'like', $slug.'%')
            ->where('id', '<>', $id)
            ->get();
    }
}
