<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
class RoleController extends Controller
{
    public function index(){
        $data = Role::all();
        return view('admin/role',['menu' => 'users', 'submenu' => 'role', 'data' => $data]);
    }

    public function create(Request $request, Role $role){
        $code = $request->c1.
                $request->r1.
                $request->u1.
                $request->d1."|".
                $request->c2.
                $request->r2.
                $request->u2.
                $request->d2."|".
                $request->c3.
                $request->r3.
                $request->u3.
                $request->d3."|".
                $request->c4.
                $request->r4.
                $request->u4.
                $request->d4."|".
                $request->c5.
                $request->r5.
                $request->u5.
                $request->d5;

        $role->role_name = $request->nama;
        $role->role_code = $code;
        if($role->save()){
            return redirect(route('users.role'))->with(['success' => 'Berhasil Menyimpan Data']);
        }else{
            return redirect(route('users.role'))->with(['failed' => 'Gagal Menyimpan Data']);
        }
    }

    public function update(Request $request, Role $roles){
        $role = $roles->find($request->id);

        $code = $request->c1.
        $request->r1.
        $request->u1.
        $request->d1."|".
        $request->c2.
        $request->r2.
        $request->u2.
        $request->d2."|".
        $request->c3.
        $request->r3.
        $request->u3.
        $request->d3."|".
        $request->c4.
        $request->r4.
        $request->u4.
        $request->d4."|".
        $request->c5.
        $request->r5.
        $request->u5.
        $request->d5;

        $role->role_name = $request->nama;
        $role->role_code = $code;
        if($role->save()){
            return redirect(route('users.role'))->with(['success' => 'Berhasil Mengupdate Data']);
        }else{
            return redirect(route('users.role'))->with(['failed' => 'Gagal Mengupdate Data']);
        }
                
    }

    public function delete(Request $request){
        $c = Role::find($request->id);
        if($c->delete()){
            return redirect(route('users.role'))->with(['success' => 'Berhasil Menghapus Data']);
        }else{
            return redirect(route('users.role'))->with(['failed' => 'Gagal Menghapus Data']);
        }
    }
}
