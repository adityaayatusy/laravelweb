<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Article;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $slide = Article::where('status','publish')->limit(3)->get();
    $content = Article::where('status','publish')->inRandomOrder()->limit(6)->get();
    return view('blog',['slide' => $slide, 'content' => $content,'menu' => 'home']);
});

Auth::routes();

Route::get('/single/{slug}', 'PostController@show')->name('single');
Route::get('/article', function(){
    $content = Article::where('status','publish')->paginate(9);
    return view('article',['content' => $content,'menu' => 'articles']);
});

Route::group(['prefix' => '/admin', 'middleware' => 'auth'], function () {
    //dashboard
    Route::get('/', function(){
        $name = User::where('id', Auth::id())->first()->name;
        $counUser = count(User::get());
        $counArticle = count(Article::get());
        $counArticleDraft = count(Article::where('status','draf')->get());
        $counArticleTrash = count(Article::onlyTrashed()->get());
        return view('admin.dashboard',['menu' => 'dashboard', 'submenu' => 'dashboard', 'name' => $name, 
        'article' => $counArticle, 'user' => $counUser, 'draft' => $counArticleDraft, 'trash' => $counArticleTrash]);
    });
    Route::get('/dashboard', function(){
        $name = User::where('id', Auth::id())->first()->name;
        $counUser = count(User::get());
        $counArticle = count(Article::get());
        $counArticleDraft = count(Article::where('status','draf')->get());
        $counArticleTrash = count(Article::onlyTrashed()->get());
        return view('admin.dashboard',['menu' => 'dashboard', 'submenu' => 'dashboard', 'name' => $name, 
        'article' => $counArticle, 'user' => $counUser, 'draft' => $counArticleDraft, 'trash' => $counArticleTrash]);
    })->name('dashboard');
    
    //post
    Route::resource('post', 'PostController');
    //category
    Route::get('/category', 'PostController@category')->name('post.category');
    Route::post('/category', 'PostController@storeCategory')->name('post.category.store');
    Route::delete('/category/{id}', 'PostController@destroyCategory')->name('post.category.destroy');
    //trash article
    Route::get('/trash', 'PostController@trash')->name('post.trash');
    Route::put('/trash/{id}', 'PostController@restore')->name('post.trash.restore');
    Route::delete('/trash/{id}', 'PostController@permanentDestroy')->name('post.trash.destroy');

    //user
    Route::resource('users', 'UserController');

    //role
    Route::get('/role', 'RoleController@index')->name('users.role');
    Route::post('/role', 'RoleController@create')->name('users.role.create');
    Route::put('/role', 'RoleController@update')->name('users.role.update');
    Route::delete('/role', 'RoleController@delete')->name('users.role.delete');
});

