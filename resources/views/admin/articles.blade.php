@extends('layouts.admin')

@section('header')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('footer')
  <!-- DataTables -->
  <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  
  <script>
    function hapus(url){
      $('#hapus').attr('action', url);
      var id = id;
        Swal.fire({
        title: 'Apa anda yakin?',
        text: "Data ini akan dihapus!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Yakin!',
        cancelButtonText: 'Batalkan'
      }).then((result) => {
        if (result.value) {
          $('#hapus').submit();
        }
      })
    }

    function restore(url){
        $('#restore').attr('action', url);
        Swal.fire({
        title: 'Apa anda yakin?',
        text: "Data ini akan dikembalikan!",
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Yakin!',
        cancelButtonText: 'Batalkan'
      }).then((result) => {
        if (result.value) {
          $('#restore').submit();
        }
      })
    }

    $(function () {
      $("#example1").DataTable({
        "scrollX": true
      });
    });

  </script>
@endsection

@section('content')
@php

    function cekRole($menu){
      
      $getRole_id = App\User::where('id', Illuminate\Support\Facades\Auth::id())->first()->role_id;
      try{
        $getCode = App\Role::where('id',$getRole_id)->first()->role_code;
      }catch(\Exception $e){
          return str_split("0100", '1');
      }
      

      $getRoleCode = explode('|', $getCode);

      switch ($menu) {
        case 'list article': return str_split($getRoleCode[0], '1'); break;
        case 'category': return str_split($getRoleCode[1], '1'); break;
        case 'trash': return str_split($getRoleCode[2], '1'); break;
        case 'users': return str_split($getRoleCode[3], '1'); break;
        case 'role': return str_split($getRoleCode[4], '1'); break;
      }

    }
@endphp
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          @if ($submenu == 'trash articles')
            <h3 class="card-title">List Trash Article</h3>
          @else
            <h3 class="card-title">List Article</h3>
            @if (cekRole('list article')[0] == "1")
              <a href="{{route('post.create')}}" class="btn btn-primary btn-sm float-sm-right"><i class="fas fa-plus"></i> Add Article</a>
            @endif
          @endif
         </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Slug</th>
              <th>Title</th>
              <th>Isi</th>
              <th>Status</th>
              <th>Thumbnail</th>
              <th>Comment</th>
              <th>Category</th>
              <th>Tag</th>
              <th>Author</th>
              <th>Create_at</th>
              <th>Update_at</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              @php $i = 1 @endphp 
              @foreach ($data as $d)
                <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{$d['slug']}}</td>
                  <td>{{$d['title']}}</td>
                  <td>{{$d['content']}}</td>
                  <td>{{$d['status']}}</td>
                  <td>
                    <img src="{{ asset('img/'.$d['thumbnail'])}}" style="max-width: 100%;height: 70px;">
                  </td>
                  <td>{{ ($d['comment'] == 1? 'True' : 'False') }}</td>
                  <td>
                    @php
                      try {
                          echo App\Category::where('id', $d['category_id'])->first()->category_name;
                      } catch (\Exception $e) {
                          echo "Default";
                      }
                   @endphp
                </td>
                  <td>{{$d['tag']}}</td>
                  <td>
                    @php
                      try {
                          echo App\User::where('id', $d['create_by'])->first()->name;
                      } catch (\Exception $e) {
                          echo "Admin";
                      }
                    @endphp
                  </td>
                  <td>{{$d['created_at']}}</td>
                  <td>{{$d['updated_at']}}</td>
                  <td>
                    @if ($submenu == 'trash articles')
                    {{-- restore --}}
                    @if (cekRole('trash')[2] == "1")
                      <button class="btn btn-primary btn-xs" onclick="restore('{{route('post.trash.restore', $d['id'])}}')">Restore</button>
                    @endif
                    @if (cekRole('trash')[3] == "1")
                      <button class="btn btn-danger btn-xs" onclick="hapus('{{route('post.trash.destroy', $d['id'])}}')">Delete</button>
                    @endif
                    @else
                      @if (cekRole('list article')[2] == "1")
                        <a href="{{ route('post.edit', $d['id'])}}" class="btn btn-success btn-xs">Edit</a>
                      @endif
                      @if (cekRole('list article')[3] == "1")
                        <button class="btn btn-danger btn-xs" onclick="hapus('{{route('post.destroy', $d['id'])}}')">Delete</button>
                      @endif
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <form action="" method="post" id="restore">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
        </form>
        <form action="" method="post" id="hapus">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
        </form>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
@endsection