@extends('layouts.admin')

@section('header')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">
  <style>

.bootstrap-tagsinput {
    background-color: #fff;
    border: 1px solid #ccc;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    display: block;
    padding: 4px 6px;
    color: #555;
    vertical-align: middle;
    border-radius: 4px;
    max-width: 100%;
    line-height: 22px;
    cursor: text;
}
.bootstrap-tagsinput input {
    border: none;
    box-shadow: none;
    outline: none;
    background-color: transparent;
    padding: 0 6px;
    margin: 0;
    width: auto;
    max-width: inherit;
}

.thumbnail {
  position: relative;
  width: 769px;
  height: 330px;
  overflow: hidden;
  margin: auto;
  background: #555;
}
.overlays{
  font-size: 100px;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  background-color: #ffffffba;
  z-index: 99;
  display: none;
}

.icon-upload{
  color: #949494;
  position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
.thumbnail:hover > .overlays{
  display: inherit;
  cursor: pointer;
}
.thumbnail img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
  </style>
@endsection

@section('footer')
  <!-- DataTables -->
  <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js') }}"></script>
  <!-- Summernote -->
  <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>

  <script>
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

    $('.overlays').click((e)=>{
      $('#img').click();
    })

    $('#img').on('change', function(){
      if (this.files && this.files[0]) { 
         var reader = new FileReader(); 
         reader.onload = function (e) { 
                 $('#thumbnail').attr('src', e.target.result); 
         }
         reader.readAsDataURL(this.files[0]); 
      }
    }) 
    
    $(function () {
      // Summernote
      $('.textarea').summernote({ height: 400})
    })
  </script>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <a href="{{route('post.index')}}" class="btn btn-danger btn-sm"><i class="fas fa-angle-left"></i> Back</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if ($submenu == 'add article')
            <form action="{{route('post.store')}}" method="post" enctype="multipart/form-data">
          @else
            <form action="{{route('post.update', $data['id'])}}" method="post"  enctype="multipart/form-data">
            {{ method_field('PUT') }}
          @endif
          
            {{ csrf_field() }}
            
            <div class="form-group">
              <label>Title</label>
  
              <div class="input-group">
              <input type="text" class="form-control" name="title" required value="{{ ($submenu == 'edit article' ? $data['title'] : '') }}">
              </div>
              <!-- /.input group -->
            </div>

            <div class="form-group">
              <label>Isi</label>

                <div class="mb-3">
                  <textarea class="textarea" name="isi" placeholder="Place some text here" style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                    {{ ($submenu == 'edit article' ? $data['content'] : '') }}
                  </textarea>
                </div>
              
              <!-- /.input group -->
            </div>

            <div class="form-group">
              <label>Category</label>
              <select class="form-control" name="category" style="width: 100%;">
                <option value='0' {{ ($submenu == 'edit article' && $data['category_id'] == 0 ? 'selected' : '') }}>Default</option>
                @foreach ($category as $c)
              <option value="{{$c['id']}}" {{ ($submenu == 'edit article' && $data['category_id'] == $c['id'] ? 'selected' : '') }}>{{$c['category_name']}}</option>
                @endforeach
               </select>
            </div>

            <div class="form-group">
              <label>Tag (tag, tag1, tag2)</label>
              <input type="text" name="tag" data-role="tagsinput" id="tags" class="form-control"  value="{{ ($submenu == 'edit article' ? $data['tag'] : '') }}">
            </div>

            <div class="form-group">
              <label>Status</label>
              <select class="form-control" name="status" style="width: 100%;">
                <option value='draf' {{ ($submenu == 'edit article' && $data['status'] == 'draf' ? 'selected' : '') }}>Draft</option>
                <option value='publish' {{ ($submenu == 'edit article' && $data['status'] == 'publish' ? 'selected' : '') }}>Published</option>
              </select>
            </div>
            {{-- comment --}}
            <div class="form-group">
              <label>Comment</label>
  
              <div class="input-group">
                <input type="checkbox" name="comment" data-bootstrap-switch="" {{ ($submenu == 'edit article' && $data['comment'] == true ? 'checked' : '') }}>
              </div>
              <!-- /.input group -->
            </div>

            <div class="form-group">
              <label>Thumbnail</label>
  
              <div class="input-group">
                <div class="thumbnail">
                  <img src="{{ ($submenu == 'edit article' ? asset('img/'.$data['thumbnail']) : asset('img/thumbnail.jpg')) }}" alt="Image" id="thumbnail" />
                  <input type="file" name="thumbnail" id="img" style="display: none">
                  <div class="overlays">
                     <i class="fas fa-camera icon-upload"></i>
                  </div>
                </div>
              </div>
              <!-- /.input group -->
            </div>
            <button type="submit" name="save" class="btn btn-primary float-sm-right" style="padding-left: 20px; padding-right:20px; ">Save</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
@endsection