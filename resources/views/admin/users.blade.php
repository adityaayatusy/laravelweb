@extends('layouts.admin')

@section('header')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('footer')
  <!-- DataTables -->
  <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  
  <script>
    function hapus(url){
      $('#hapus').attr('action', url);
      var id = id;
        Swal.fire({
        title: 'Apa anda yakin?',
        text: "Data ini akan dihapus!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Yakin!',
        cancelButtonText: 'Batalkan'
      }).then((result) => {
        if (result.value) {
          $('#hapus').submit();
        }
      })
    }

    function restore(id){
      var id = id;
        Swal.fire({
        title: 'Apa anda yakin?',
        text: "Data ini akan dikembalikan!",
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Yakin!',
        cancelButtonText: 'Batalkan'
      }).then((result) => {
        if (result.value) {
          $('#restore').submit();
        }
      })
    }

    $(function () {
      $("#example1").DataTable({
        "scrollX": true
      });
    });

  </script>
@endsection

@section('content')
@php

    function cekRole($menu){
      
      $getRole_id = App\User::where('id', Illuminate\Support\Facades\Auth::id())->first()->role_id;
      try{
        $getCode = App\Role::where('id',$getRole_id)->first()->role_code;
      }catch(\Exception $e){
          return str_split("0100", '1');
      }
      

      $getRoleCode = explode('|', $getCode);

      switch ($menu) {
        case 'list article': return str_split($getRoleCode[0], '1'); break;
        case 'category': return str_split($getRoleCode[1], '1'); break;
        case 'trash': return str_split($getRoleCode[2], '1'); break;
        case 'users': return str_split($getRoleCode[3], '1'); break;
        case 'role': return str_split($getRoleCode[4], '1'); break;
      }

    }
@endphp

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
            <h3 class="card-title">List User</h3>
            @if (cekRole('category')[0] == "1")
              <a href="{{route('users.create')}}" class="btn btn-primary btn-sm float-sm-right"><i class="fas fa-plus"></i> Add User</a>
            @endif
          </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Password</th>
              <th>Role</th>
              <th>Create_at</th>
              <th>Update_at</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              @php $i = 1 @endphp 
              @foreach ($data as $d)
                <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{$d['name']}}</td>
                  <td>{{$d['email']}}</td>
                  <td>{{$d['password']}}</td>
                  <td>
                    @php
                      try {
                          echo App\Role::where('id', $d['role_id'])->first()->role_name;
                      } catch (\Exception $e) {
                          echo "Member";
                      }
                   @endphp
                  </td>
                  <td>{{$d['created_at']}}</td>
                  <td>{{$d['updated_at']}}</td>
                  <td>
                    @if (cekRole('category')[2] == "1")
                      <a href="{{ route('users.edit', $d['id'])}}" class="btn btn-success btn-xs">Edit</a>
                    @endif
                    @if (cekRole('category')[3] == "1")
                      <button class="btn btn-danger btn-xs" onclick="hapus('{{route('users.destroy', $d['id'])}}')">Delete</button>
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <form action="" method="post" id="hapus">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
          </form>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
@endsection