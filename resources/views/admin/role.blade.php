@extends('layouts.admin')

@section('header')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <style>
    .btns{
      cursor: pointer;
    }
    .noselect {
      -webkit-touch-callout: none; /* iOS Safari */
        -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
          -moz-user-select: none; /* Old versions of Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
                user-select: none; /* Non-prefixed version, currently
                                      supported by Chrome, Edge, Opera and Firefox */
    }
  </style>
@endsection

@section('footer')
  <!-- DataTables -->
  <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  
  <script>
    function hapus(){
      var id = id;
        Swal.fire({
        title: 'Apa anda yakin?',
        text: "Data ini akan dihapus!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Yakin!',
        cancelButtonText: 'Batalkan'
      }).then((result) => {
        if (result.value) {
          del();
        }
      })
    }

    function allcheck(){
      $('input[type="checkbox"]').prop('checked', true);
    }
    function alluncheck(){
      $('input[type="checkbox"]').prop('checked', false);
    }

    $( "#checkedCreate" ).click(function() {
      if($(this).attr('data-create-state') == 1) {
        $(this).attr('data-create-state', 0);
        $('.create').prop('checked', false);
      }
      else {
        $(this).attr('data-create-state', 1);
        $('.create').prop('checked', true);
      }
    });

    $( "#checkedRead" ).click(function() {
      if($(this).attr('data-read-state') == 1) {
        $(this).attr('data-read-state', 0);
        $('.read').prop('checked', false);
      }
      else {
        $(this).attr('data-read-state', 1);
        $('.read').prop('checked', true);
      }
    });

    $( "#checkedUpdate" ).click(function() {
      if($(this).attr('data-update-state') == 1) {
        $(this).attr('data-update-state', 0);
        $('.update').prop('checked', false);
      }
      else {
        $(this).attr('data-update-state', 1);
        $('.update').prop('checked', true);
      }
    });

    $( "#checkedDelete" ).click(function() {
      if($(this).attr('data-delete-state') == 1) {
        $(this).attr('data-delete-state', 0);
        $('.delete').prop('checked', false);
      }
      else {
        $(this).attr('data-delete-state', 1);
        $('.delete').prop('checked', true);
      }
    });
    


    function save(){
      $('#trigger').trigger('click');
    }

    $('#listRole').on('input', function(){
      $('#btn-edit').prop('disabled', false);
      $('#btn-del').prop('disabled', false);
      console.log();
      $('#nama').val($("#listRole option:selected").attr("role-name"))
      translateCode($("#listRole option:selected").attr("role-code"))
    })

    function update(){
      $('#form').append('<input type="hidden" name="_method" value="PUT">');
      $('#form').append('<input type="hidden" name="id" value="'+$("#listRole option:selected").attr("value")+'">');
      
      $('#trigger').trigger('click');
    }

    function del(){
      $('#form').append('<input type="hidden" name="_method" value="DELETE">');
      $('#form').append('<input type="hidden" name="id" value="'+$("#listRole option:selected").attr("value")+'">');
      
      $('#trigger').trigger('click');
    }

    function translateCode(code){
      var arr = code.split("|");
      for(var i= 0;i< arr.length;i++){
        var perm = arr[i].split("");
        var n = i+1;
        $('#c'+n).prop('checked', Boolean(Number(perm[0])));
        $('#r'+n).prop('checked', Boolean(Number(perm[1])));
        $('#u'+n).prop('checked', Boolean(Number(perm[2])));
        $('#d'+n).prop('checked', Boolean(Number(perm[3])));
      }
    }
  </script>

@endsection

@section('content')
@php

    function cekRole($menu){
      
      $getRole_id = App\User::where('id', Illuminate\Support\Facades\Auth::id())->first()->role_id;
      try{
        $getCode = App\Role::where('id',$getRole_id)->first()->role_code;
      }catch(\Exception $e){
          return str_split("0100", '1');
      }
      

      $getRoleCode = explode('|', $getCode);

      switch ($menu) {
        case 'list article': return str_split($getRoleCode[0], '1'); break;
        case 'category': return str_split($getRoleCode[1], '1'); break;
        case 'trash': return str_split($getRoleCode[2], '1'); break;
        case 'users': return str_split($getRoleCode[3], '1'); break;
        case 'role': return str_split($getRoleCode[4], '1'); break;
      }

    }
@endphp

<div class="container-fluid">
  <div class="row">
    <div class="col-6">
      <div class="card">
        <div class="card-header">
          <label>Role</label>
         </div>
        <!-- /.card-header -->
        <div class="card-body">
          <form action="{{route('users.role.create')}}" method="post" id="form">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="nama">Nama Role:</label>
              <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Role" required>
            </div>
            <table class="table mb-4">
              <thead>
                <tr>
                  <th>Menu</th>
                  <th class="btns noselect" id="checkedCreate">Create</th>
                  <th class="btns noselect" id="checkedRead">Read</th>
                  <th class="btns noselect" id="checkedUpdate">Update</th>
                  <th class="btns noselect" id="checkedDelete">Delete</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>List Article</th>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='c1'>
                    <input class="form-check-input mx-auto create" type="checkbox" value="1" data-state='0' name='c1' id="c1">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='r1'>
                    <input class="form-check-input mx-auto read" type="checkbox" value="1" data-read-state='0' name='r1' id="r1">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='u1'>
                    <input class="form-check-input mx-auto update" type="checkbox" value="1" data-update-state='0' name='u1' id="u1">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='d1'>
                    <input class="form-check-input mx-auto delete" type="checkbox" value="1" data-delete-state='0' name='d1' id="d1">
                  </td>
                </tr>
                <tr>
                  <th>Category</th>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='c2'>
                    <input class="form-check-input mx-auto create" type="checkbox" value="1" data-state='0' name='c2' id="c2">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='r2'>
                    <input class="form-check-input mx-auto read" type="checkbox" value="1" data-read-state='0' name='r2' id="r2">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='u2'>
                    <input class="form-check-input mx-auto update" type="checkbox" value="1" data-update-state='0' name='u2' id="u2">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='d2'>
                    <input class="form-check-input mx-auto delete" type="checkbox" value="1" data-delete-state='0' name='d2' id="d2">
                  </td>
                </tr>
                <tr>
                  <th>Trash</th>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='c3'>
                    <input class="form-check-input mx-auto create" type="checkbox" value="1" data-state='0' name='c3' id="c3">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='r3'>
                    <input class="form-check-input mx-auto read" type="checkbox" value="1" data-read-state='0' name='r3' id="r3">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='u3'>
                    <input class="form-check-input mx-auto update" type="checkbox" value="1" data-update-state='0' name='u3' id="u3">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='d3'>
                    <input class="form-check-input mx-auto delete" type="checkbox" value="1" data-delete-state='0' name='d3' id="d3">
                  </td>
                </tr>
                <tr>
                  <th>Users</th>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='c4'>
                    <input class="form-check-input mx-auto create" type="checkbox" value="1" data-state='0' name='c4' id="c4">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='r4'>
                    <input class="form-check-input mx-auto read" type="checkbox" value="1" data-read-state='0' name='r4' id="r4">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='u4'>
                    <input class="form-check-input mx-auto update" type="checkbox" value="1" data-update-state='0' name='u4' id="u4">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='d4'>
                    <input class="form-check-input mx-auto delete" type="checkbox" value="1" data-delete-state='0' name='d4' id="d4">
                  </td>
                </tr>
                <tr>
                  <th>Role</th>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='c5'>
                    <input class="form-check-input mx-auto create" type="checkbox" value="1" data-state='0' name='c5' id="c5">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='r5'>
                    <input class="form-check-input mx-auto read" type="checkbox" value="1" data-read-state='0' name='r5' id="r5">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='u5'>
                    <input class="form-check-input mx-auto update" type="checkbox" value="1" data-update-state='0' name='u5' id="u5">
                  </td>
                  <td>
                    <input class="form-check-input mx-auto" type="hidden" value="0" name='d5'>
                    <input class="form-check-input mx-auto delete" type="checkbox" value="1" data-delete-state='0' name='d5' id="d5">
                  </td>
                </tr>
              </tbody>
              
            </table>
            <button type="submit" id="trigger" style="display: none;"></button>
          </form>
          <div class="float-sm-left">
            <button type="button" class="btn btn-primary btn-sm" id="btn-all" onclick="allcheck()">Select All</button>
            <button type="button" class="btn btn-secondary btn-sm" id="btn-all" onclick="alluncheck()">Unselect All</button>
          </div>
          <div class="float-sm-right">
            @if (cekRole('category')[0] == "1")
              <button type="button" class="btn btn-success btn-sm" id="btn-save" onclick="save()">Save New</button>
            @endif
            @if (cekRole('category')[2] == "1")
              <button type="button" class="btn btn-warning btn-sm" id="btn-edit" onclick="update()" disabled>Save Edit</button>
            @endif
            @if (cekRole('category')[3] == "1")
              <button type="button" class="btn btn-danger btn-sm" id="btn-del" onclick="hapus()"  disabled>Delete</button>
            @endif
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <div class="col-6">
      <div class="card">
        <div class="card-header">
          <div class="form-group">
            <label>List Role</label>
          </div>
         </div>
        <!-- /.card-header -->
        <div class="card-body">
          <div class="form-group">
            <select multiple class="form-control" size="19" id="listRole">
              @foreach ($data as $d)
                <option value="{{$d['id']}}" role-name="{{$d['role_name']}}" role-code="{{$d['role_code']}}">{{$d['role_name']}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
@endsection