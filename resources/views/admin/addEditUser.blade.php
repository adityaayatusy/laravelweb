@extends('layouts.admin')

@section('header')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">
    <style>
      .hide{
        display: none;
      }
    </style>
@endsection

@section('footer')
  <!-- DataTables -->
  <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js') }}"></script>
  <!-- Summernote -->
  <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>

  <script>
    $('#pw').on('input', function(){
      $('.alert-pw').hide();
    })

    $('#repw').on('input', function(){
      $('.alert-pw').hide();
    })

    $('#show').click(function(){
      $('#show').hide()
      $('#foldpw').show();
      $('#fpw').show();
      $('#frepw').show();
      $('#pw').prop('required',true);
      $('#repw').prop('required',true);
      $('#oldpw').prop('required',true);
    })

    function kirim(){
        var pw = $('#pw').val();
        var repw = $('#repw').val();

        if(pw == repw){
          $('#save').click();
        }else{
          $('.alert-pw').show();
        }
    }
  </script>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <a href="{{route('users.index')}}" class="btn btn-danger btn-sm"><i class="fas fa-angle-left"></i> Back</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if ($submenu == 'add user')
            <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
          @else
            <form action="{{route('users.update', $data['id'])}}" method="post"  enctype="multipart/form-data">
            {{ method_field('PUT') }}
          @endif
          
            {{ csrf_field() }}

            <div class="form-group">
              <label>Name</label>
  
              <div class="input-group">
                <input type="text" class="form-control" name="name" required value="{{ ($submenu == 'edit user' ? $data['name'] : '') }}">
              </div>
              <!-- /.input group -->
            </div>

            <div class="form-group">
              <label>Email</label>
  
              <div class="input-group">
                <input type="email" class="form-control" name="email" required value="{{ ($submenu == 'edit user' ? $data['email'] : '') }}">
              </div>
              <!-- /.input group -->
            </div>

            <div class="form-group">
              <label>Role</label>
              <select class="form-control" name="role" style="width: 100%;">
                <option value='0' {{ ($submenu == 'edit user' && $data['role_id'] == 0 ? 'selected' : '') }}>Default</option>
                @foreach ($roles as $r)
              <option value="{{$r['id']}}" {{ ($submenu == 'edit user' && $data['role_id'] == $r['id'] ? 'selected' : '') }}>{{$r['role_name']}}</option>
                @endforeach
              </select>
            </div>
            @if ($submenu == 'edit user')
            <div class="form-group">
              <div class="input-group">
                <button name="show" id="show" type="button" class="btn btn-warning float-sm-right" style="padding-left: 20px; padding-right:20px; ">Edit Password</button>
              </div>
            </div>
            <div class="form-group {{ ($submenu == 'edit user'? 'hide':'' )}}" id="foldpw">
              <label>Old-Password</label>
  
              <div class="input-group">
                <input type="password'" class="form-control" name="oldpassword" {{ ($submenu == 'edit user'? '':'reqired' )}} id="oldpw">
              </div>
            </div>
            @endif
            <div class="form-group {{ ($submenu == 'edit user'? 'hide':'' )}}" id="fpw">
              <label>Password</label>
  
              <div class="input-group">
                <input type="password" class="form-control" name="password" {{ ($submenu == 'edit user'? '':'reqired' )}} id="pw">
                
              </div>
              <div class="text-danger alert-pw hide">Password tidak sama</div>
              <!-- /.input group -->
            </div>
            <div class="form-group {{ ($submenu == 'edit user'? 'hide':'' )}}" id="frepw">
              <label>Re-Password</label>
  
              <div class="input-group">
                <input type="password" class="form-control" name="repassword" {{ ($submenu == 'edit user'? '':'reqired' )}} id="repw">
                
              </div>
              <div class="text-danger alert-pw hide">Password tidak sama</div>
              <!-- /.input group -->
            </div>
            <button type="submit" name="save" style="display: none" class="btn btn-primary float-sm-right" style="padding-left: 20px; padding-right:20px; " id="save"></button>
          
          </form>
          <button  class="btn btn-primary float-sm-right" style="padding-left: 20px; padding-right:20px;" onclick="kirim()">Save</button>
          
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
@endsection