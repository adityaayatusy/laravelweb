@php
if (!function_exists('cekRole')) {
    function cekRole($menu){
      
      $getRole_id = App\User::where('id', Illuminate\Support\Facades\Auth::id())->first()->role_id;
      try{
        $getCode = App\Role::where('id',$getRole_id)->first()->role_code;
      }catch(\Exception $e){
          return str_split("0100", '1');
      }
      

      $getRoleCode = explode('|', $getCode);

      switch ($menu) {
        case 'list article': return str_split($getRoleCode[0], '1'); break;
        case 'category': return str_split($getRoleCode[1], '1'); break;
        case 'trash': return str_split($getRoleCode[2], '1'); break;
        case 'users': return str_split($getRoleCode[3], '1'); break;
        case 'role': return str_split($getRoleCode[4], '1'); break;
      }

    }
}
@endphp

<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Admin | {{ ucwords($submenu) }}</title>
    
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
        .nav .nav-treeview .nav-item{
            padding-left: 10px;
        }
    </style>
    @yield('header')
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    <!-- profile -->
      <li class="nav-item mr-2">
        <div class="user-panel d-flex">
            <div class="info">
              <span class="d-block">
                {{ ucwords(App\User::where('id', Illuminate\Support\Facades\Auth::id())->first()->name) }}
              </span>
            </div>
          </div>
      </li>
      <li class="nav-item">
        <button class="btn btn-block btn-danger btn-sm" onclick="confirmLogout()">
            <i class="fas fa-power-off"></i>
        </button>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin Website</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
          <a href="{{route('dashboard')}}" class="nav-link {{ ($menu == 'dashboard' ? 'active' : '') }}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
            </a>
          </li>
          @if (cekRole('list article')[1] == "1" || cekRole('category')[1] == "1" || cekRole('trash')[1] == "1")
          <li class="nav-item has-treeview {{ ($menu == 'articles' ? 'menu-open' : '') }}">
            <a href="#" class="nav-link {{ ($menu == 'articles' ? 'active' : '') }}">
              <i class="nav-icon fas fa-newspaper"></i>
              <p>
                Acticles
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              @if (cekRole('list article')[1] == "1")
                <li class="nav-item">
                  <a href="{{route('post.index')}}" class="nav-link {{ ($submenu == 'articles' || $submenu == 'add article' || $submenu == 'edit article' ? 'active' : '') }}">
                    <i class="fas fa-list-alt nav-icon"></i>
                    <p>List Article</p>
                  </a>
                </li>
              @endif
              @if (cekRole('category')[1] == "1")
              <li class="nav-item">
                <a href="{{route('post.category')}}" class="nav-link {{ ($submenu == 'category' ? 'active' : '') }}">
                  <i class="fas fa-th nav-icon"></i>
                  <p>Category</p>
                </a>
              </li>
              @endif
              @if (cekRole('trash')[1] == "1")
              <li class="nav-item">
                <a href="{{route('post.trash')}}" class="nav-link {{ ($submenu == 'trash articles' ? 'active' : '') }}">
                  <i class="far fa-trash-alt nav-icon"></i>
                  <p>Trash</p>
                </a>
              </li>
              @endif
            </ul>
          </li>
          @endif
          @if (cekRole('users')[1] == "1" || cekRole('role')[1] == "1")
            <li class="nav-item has-treeview {{ ($menu == 'users' ? 'menu-open' : '') }}">
              <a href="#" class="nav-link {{ ($menu == 'users' ? 'active' : '') }}">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  Users
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                @if (cekRole('users')[1] == "1")
                <li class="nav-item">
                  <a href="{{route('users.index')}}" class="nav-link {{ ($submenu == 'users'|| $submenu == 'add user' || $submenu == 'edit user' ? 'active' : '') }}">
                    <i class="fas fa-user nav-icon"></i>
                    <p>List Users</p>
                  </a>
                </li>
                @endif
                @if (cekRole('role')[1] == "1")
                <li class="nav-item">
                  <a href="{{route('users.role')}}" class="nav-link {{ ($submenu == 'role' ? 'active' : '') }}">
                    <i class="fas fa-user-tag nav-icon"></i>
                    <p>Role</p>
                  </a>
                </li>
                @endif
              </ul>
            </li>
          @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ ucwords($submenu) }}</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; {{date('Y')}}.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> Beta
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<!-- Toastr -->
<script src="{{ asset('plugins/toastr/toastr.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js')}}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{ asset('dist/js/demo.js')}}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{ asset('plugins/raphael/raphael.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js')}}"></script>

<!-- PAGE SCRIPTS -->
<script src="{{ asset('dist/js/pages/dashboard2.js')}}"></script>

<script>
    function confirmLogout(){
        Swal.fire({
            title: 'Apa anda yakin ingin keluar?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, saya ingin keluar',
            cancelButtonText: 'Batalkan'
            }).then((result) => {
            if (result.value) {
                $('#logout-form').submit(); 
            }
        })
    }

    
</script>
@if (session('success'))
  <script> toastr.success("{{session('success')}}"); </script>
@elseif(session('failed'))
  <script> toastr.error("{{session('failed')}}"); </script>
@endif  
@yield('footer')

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
</form>
</body>
</html>