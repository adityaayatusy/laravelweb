@extends('layouts.app')

@section('content')
    <div class="container mt-2" style="padding-bottom:80px;min-heigth:400px">
        <h3 style="font-weight: bold">{{ucwords($data['title'])}}</h3>
        <span style="font-size: 14px;">{{date("d M Y", strtotime($data['created_at']))}}, {{App\User::where('id', $data['create_by'])->first()->name}}</span>
        <img src="{{asset('img/'.$data['thumbnail'])}}" class="d-block w-100 thumbnail mt-3 mb-3" alt="{{asset('img/'.$data['thumbnail'])}}">
        <div class="content mt-5 mb-5">
            {!!$data['content']!!}
        </div>
        @if ($data['comment'])
            <div id="disqus_thread"></div>
            <script>

            /**
            *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
            *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
        
            var disqus_config = function () {
                this.page.url = '{{route('single',$data['slug'])}}';  // Replace PAGE_URL with your page's canonical URL variable
                this.page.identifier = '{{ $data['slug'] }}' ; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
            };

            (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://blog-znqkakt1pz.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
            })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            <script id="dsq-count-scr" src="//blog-znqkakt1pz.disqus.com/count.js" async></script> 
        @else
            <h4 class="text-center">Comment disabled</h4>
        @endif
                    
    </div>
@endsection