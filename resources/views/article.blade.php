@extends('layouts.app')

@section('content')
    <div class="container mt-2" >
      <h3 class="text-center m-5">Articles</h3>
          <div class="d-flex">
            @foreach ($content as $c)
              <div class="card mt-5 m-auto" style="width: 18rem;">
                  <img src="{{asset('img/'.$c['thumbnail'])}}" class="card-img-top" alt="...">
                  <div class="card-body">
                  <h5 class="card-title">{{$c['title']}}</h5>
                  <p class="card-text">{{strip_tags($c['content'], 20)}}</p>
                    <a href="{{route('single',$c['slug'])}}" class="btn btn-primary">Read More</a>
                  </div>
              </div>
            @endforeach
            </div>
            <div class="float">
              {{ $content->links() }}
            </div>
            
    </div>
@endsection