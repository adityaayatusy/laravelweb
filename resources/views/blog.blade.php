@extends('layouts.app')

@section('content')
    <div class="container mt-2" style="padding-bottom:80px;">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              @foreach ($slide as $key => $s)
              <li data-target="#carouselExampleCaptions" data-slide-to="{{$key}}" class="{{ ($key == 0 ? 'active' : '')}}"></li>
              @endforeach
            </ol>
            
            <div class="carousel-inner">
              @foreach ($slide as $key => $s)
              <div class="carousel-item {{ ($key == 0 ? 'active' : '')}} ">
                <img src="{{asset('img/'.$s['thumbnail'])}}" class="d-block w-100 thumbnail" alt="{{asset('img/'.$s['thumbnail'])}}">
                <div class="carousel-caption d-none d-md-block">
                  <h5>{{$s['title']}}</h5>
                  <a href="{{route('single',$s['slug'])}}" class="btn btn-primary">Read More</a>
                </div>
              </div>
              @endforeach
        
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <div class="d-flex justify-content-between flex-wrap">
            @foreach ($content as $c)
              <div class="card mt-5" style="width: 18rem;">
                  <img src="{{asset('img/'.$c['thumbnail'])}}" class="card-img-top" alt="...">
                  <div class="card-body">
                  <h5 class="card-title">{{$c['title']}}</h5>
                  <p class="card-text">{{strip_tags($c['content'], 20)}}</p>
                    <a href="{{route('single',$c['slug'])}}" class="btn btn-primary">Read More</a>
                  </div>
              </div>
            @endforeach
        </div>
    </div>
@endsection